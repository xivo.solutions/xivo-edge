#!/usr/bin/env bash
set -e

docker build --build-arg TARGET_VERSION=$TARGET_VERSION -t ${DOCKER_REPOSITORY}:$TARGET_VERSION .

