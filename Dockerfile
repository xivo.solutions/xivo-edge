FROM alpine

ARG TARGET_VERSION
LABEL version=${TARGET_VERSION}

RUN mkdir -p /etc/xivo-edge
ADD *.yml /etc/xivo-edge/