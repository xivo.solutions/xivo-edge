# XiVO Edge proxy

## NGINX folder
Those NGINX files are available on the VM and used as a template for the proxy NGINX docker container.

### For more details, see - [R&D Edge wiki](https://gitlab.com/avencall/randd-doc/-/wikis/infrastructure/stun-turn-dev#launch-services)